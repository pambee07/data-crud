INSERT INTO book VALUES (null,'Moby Dick', "The book is the sailor Ishmael's narrative of the obsessive quest of Ahab, captain of the whaling ship Pequod, for revenge against Moby Dick, the giant white whale that on the ship's previous voyage bit off Ahab's leg at the knee.", "1851-10-18",'Herman Melville',1);

                                                                                                            INSERT INTO book VALUES (null, 'The Great Gatsby' , "Set in Jazz Age New York, it tells the tragic story of Jay Gatsby, a self-made millionaire, and his pursuit of Daisy Buchanan, a wealthy young woman whom he loved in his youth.","1925-04-10",'F. Scott Fitzgerald',1);                                                                                                         INSERT INTO book VALUES (null, 'Frankenstein' , "Victor Frankenstein tells Walton his story—a happy childhood, an unhealthy obsession with alchemy, and his engagement to his cousin Elizabeth. Victor enrolls at the University of Ingolstadt, where he discovers the secret of life and builds a creature from dead bodies.","1818-01-01",'Mary Shelley',0);                                                   
                                                      INSERT INTO book VALUES ( null, 'Robinson Crusoe',null,"1719-04-25",'Daniel Defoe',1);                                                             
SELECT * FROM book;

SELECT * FROM book WHERE authors='Herman Melville';

SELECT * FROM book WHERE description IS null;

UPDATE book SET description="Crusoe is on a ship bound for Africa, where he plans to buy slaves for his plantations in South America, when the ship is wrecked on an island and Crusoe is the only survivor. Alone on a desert island, Crusoe manages to survive thanks to his pluck and pragmatism." WHERE description IS null;

DELETE FROM book WHERE available = 0;

